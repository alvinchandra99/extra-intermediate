public class Traffic extends Thread{
    private int startTime;
    private String trafficName;
    private boolean statusRedLight;
    private static int redLight = 120;
    private static int orangeLight = 5;

    Traffic(int startTime, String trafficName){
        this.startTime = startTime;
        this.trafficName = trafficName;
    }
    public boolean getStatusRedLight(){
        return this.statusRedLight;
    }
    public void setStatusTrafficLight(boolean statusRedLight){
        this.statusRedLight = statusRedLight;
    }

    @Override
    public void run(){
        if(startTime > 0){
            try{
                Thread.sleep(100 * startTime);
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }            
        }
    
        while(true){
            try{
                setStatusTrafficLight(true);
                System.out.println(trafficName + " = Lampu Merah Menyala");
                Thread.sleep((redLight - orangeLight ) * 100);
                System.out.println(this.trafficName + " = Lampu Orange Menyala");
                Thread.sleep(100 * 5);
                setStatusTrafficLight(false);
                System.out.println(this.trafficName + " = Lampu Hijau Menyala ");
                Thread.sleep(100 * 35);
                System.out.println(this.trafficName + " = Lampu Orang Menyala");
            }
            catch(InterruptedException e){
                e.printStackTrace();
            }
            
        }

    
    }

}