import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Traffic T1 = new Traffic(0, "T1");
        Traffic T2 = new Traffic(40, "T2");
        Traffic T3 = new Traffic(80, "T3");
        T1.start();
        T2.start();
        T3.start();
        
        Scanner userInput = new Scanner(System.in);
        System.out.println("Traffic asal : ");
        int fromTraffic = userInput.nextInt();
        System.out.println("Traffic tujuan : ");
        int toTraffic = userInput.nextInt();

        // Traffic Dari T1
        if(fromTraffic == 1 && toTraffic == 2 ){
            System.out.println("Silahkan Jalan, Tidak Perlu Menunggu Lampu Merah");
        }
        
        if(fromTraffic == 1 && toTraffic == 3  && T1.getStatusRedLight() == true){
            System.out.println("Silahkan Menunggu Lampu Merah Berganti");
        }
        if(fromTraffic == 1 && toTraffic == 3  && T1.getStatusRedLight() == false){
            System.out.println("Silahkan Lewat");
        }

        // Traffic Dari T2
        if(fromTraffic == 2 && toTraffic == 3 ){
            System.out.println("Silahkan Jalan, Tidak Perlu Menunggu Lampu Merah");
        }
        if(fromTraffic == 2 && toTraffic == 1  && T2.getStatusRedLight() == true){
            System.out.println("Silahkan Menunggu Lampu Merah Berganti");
        }
        if(fromTraffic == 2 && toTraffic == 1  && T2.getStatusRedLight() == false){
            System.out.println("Silahkan Lewat");
        }

        // Traffic Dari T3
        if(fromTraffic == 3 && toTraffic == 1 ){
            System.out.println("Silahkan Jalan, Tidak Perlu Menunggu Lampu Merah");
        }
        if(fromTraffic == 3 && toTraffic == 2  && T3.getStatusRedLight() == true){
            System.out.println("Silahkan Menunggu Lampu Merah Berganti");
        }
        if(fromTraffic == 3 && toTraffic == 2  && T3.getStatusRedLight() == false){
            System.out.println("Silahkan Lewat");
        }

        
    }    
    
}
